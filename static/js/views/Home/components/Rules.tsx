import React from 'react'
import { Card, CardBody, Heading, Text, Link, LinkExternal } from 'uikit'
import BigNumber from 'bignumber.js/bignumber'
import styled from 'styled-components'
import { Timeline } from 'react-twitter-widgets'
import { getBalanceNumber } from 'utils/formatBalance'
import { useTotalSupply, useBurnedBalance } from 'hooks/useTokenBalance'
import useI18n from 'hooks/useI18n'
import { getCakeAddress } from 'utils/addressHelpers'
import CardValue from './CardValue'
import { useFarms } from '../../../state/hooks'

const StyledTwitterCard = styled(Card)`
${({ theme }) => theme.mediaQueries.lg} {
	-webkit-box-align: center;
    align-items: center;
    background-image: url(/images/egg/header-forest-bg.png);
    background-position: center center, center center;
    background-size: cover;
    background-repeat: no-repeat;
    box-shadow: rgb(31 43 70 / 35%) 0px 0px 0px 3000px inset;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    flex-direction: column;
    margin: auto auto 35px;
    padding: 32px 16px;
    text-align: center;
    box-sizing: border-box;
  }
  margin-left: auto;
  margin-right: auto;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  margin-bottom: 8px;
`
const StyledLinkExternal = styled(LinkExternal)`
  text-decoration: none;
  font-weight: normal;
  color: ${({ theme }) => theme.colors.text};
  display: flex;
  align-items: center;
  svg {
    padding-left: 4px;
    height: 18px;
    width: auto;
    fill: ${({ theme }) => theme.colors.primary};
  }
`

const TwitterCard = () => {
  const TranslateString = useI18n()
  
  const howitworks = "Information & Rules"
  const linkdetails = "Read more here"
  const linkaddress = "https://jaguarswap.gitbook.io/jaguarswap/"
  
  return (
    <StyledTwitterCard>
      <CardBody>
        <Heading size="xl" mb="24px">
          {howitworks}
        </Heading>

        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>Buyback Burning Mechanism</li>
        </ul>               
        
        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>On-chain Referral Program</li>
        </ul>   
        
        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>Emission Reduction</li>
        </ul>   
        
        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>Liquidity will not be withdrawn</li>
        </ul>   

        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>Transfer Tax Burning</li>
        </ul>                  
  
        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>1/4 of the deposit fee used for Buybacks</li>
        </ul>
        
        <div style={{height: "10px"}}>&nbsp;</div>
        <ul>
        <li>Migrator Code Removed</li>
        </ul>
        

        <div style={{height: "10px"}}>&nbsp;</div>
        <div style={{fontWeight: "bold"}}>
        <ul>
        <li>HIGH RISK. Understand the risks!</li>
          </ul>
        </div> 
        
        <div style={{height: "20px"}}>&nbsp;</div>
        
       <>
          <StyledLinkExternal href={linkaddress}>
            {linkdetails}
          </StyledLinkExternal>
        </>





      </CardBody>
    </StyledTwitterCard>
  )
}

export default TwitterCard
