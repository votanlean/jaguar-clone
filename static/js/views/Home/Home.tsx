import React from 'react'
import styled from 'styled-components'
import Cookies from 'universal-cookie';
import { Heading, Text, BaseLayout } from 'uikit'
import useI18n from 'hooks/useI18n'
import Page from 'components/layout/Page'
import { useQueryParam, StringParam } from 'use-query-params';
import FarmStakingCard from './components/FarmStakingCard'
import MetaMask from './components/MetaMask'
import Marketcap from './components/Marketcap'
import rot13 from '../../utils/encode'
import LotteryCard from './components/LotteryCard'
import { isAddress } from '../../utils/web3'
import CakeStats from './components/CakeStats'
import TotalValueLockedCard from './components/TotalValueLockedCard'
import TwitterCard from './components/TwitterCard'
import Rules from './components/Rules'
import CountDown from "./CountDown";

const Hero = styled.div`
  align-items: center;
  background-image: url('/images/egg/fake.png');
  background-repeat: no-repeat;
  background-position: top center;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  margin-bottom: 32px;
  padding-top: 116px;
  text-align: center;

  ${({ theme }) => theme.mediaQueries.lg} {
	-webkit-box-align: center;
    align-items: center;
    background-image: url(/images/egg/header-forest-bg.png);
    background-position: center center, center center;
    background-size: cover;
    background-repeat: no-repeat;
    box-shadow: rgb(31 43 70 / 35%) 0px 0px 0px 3000px inset;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    flex-direction: column;
    margin: auto auto 35px;
    padding: 32px 16px;
    text-align: center;
    box-sizing: border-box;
  }
`



	
const Cards = styled(BaseLayout)`
  align-items: stretch;
  justify-content: stretch;
  margin-bottom: 48px;

  & > div {
    grid-column: span 6;
    width: 100%;
  }

  ${({ theme }) => theme.mediaQueries.sm} {
    & > div {
      grid-column: span 8;
    }
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    & > div {
      grid-column: span 6;
    }
  }
`

const Home: React.FC = () => {
  const TranslateString = useI18n()
  const cookies = new Cookies();
  const [ref, setNum] = useQueryParam('ref', StringParam);

  if(ref) {
    if(isAddress(rot13(ref))) {
      cookies.set("ref", ref)
    }
  }

  return (
    <Page>
	<CountDown/>
      <Hero>
        <Heading as="h1" size="xl" mb="24px" color="secondary">
          {TranslateString(576, 'JaguarSwap')}
        </Heading>
        <Text>{TranslateString(578, 'Yield Farm & Algorithmic Stablecoin Protocol & AMM on #BSC')}</Text>
        <Text>{TranslateString(578, 'JaguarSwap - Yield Farming on Binance Smart Chain')}</Text>
      </Hero>
      <div>
        <Cards>
          <FarmStakingCard />
		  <LotteryCard />
		  <Marketcap />
          <TotalValueLockedCard />
		  <MetaMask />
		  <CakeStats />
		  <Rules />
          <TwitterCard/>
        </Cards>
      </div>
    </Page>
  )
}

export default Home
