export default {
  cake: {
    56: '0x4a3524936Db5C310d852266033589D3f6F30BA5d',
    97: '',
  }, 

  masterChef: {
    56: '0x8e4301509A484c6fC211C8902013e90cD416F58D',
    97: '',
  },
  wbnb: {
    56: '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c',
    97: '',
  },
  lottery: {
    56: '0x4ABC4FDCd39de6419A65BDC365E3f6b6a47a85dc',
    97: '',
  },
  lotteryNFT: {
    56: '0x526158c48f8611BA8f359d5d4b548686F76F6c92',
    97: '',
  },
  mulltiCall: {
    56: '0x1ee38d535d541c55c9dae27b12edf090c608e6fb',
    97: '0x67ADCB4dF3931b0C5Da724058ADC2174a9844412',
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
  },
  referral: {
    56: '0x43EE4A63720b3D114638aE7678aC405f8Fafd578',
    97: '',
  }
}
