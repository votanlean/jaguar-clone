import { MenuEntry } from 'uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange LP V2',
        href: 'https://exchange.pancakeswap.finance/#/swap?inputCurrency=ETH&outputCurrency=0x4a3524936Db5C310d852266033589D3f6F30BA5d',
      },
      {
        label: 'Liquidity LP V2',
        href: 'https://exchange.pancakeswap.finance/#/add/0x4a3524936Db5C310d852266033589D3f6F30BA5d/BNB',
      },
	  {
        label: 'Exchange LP V1',
        href: 'https://exchange.goosedefi.com/#/swap?inputCurrency=ETH&outputCurrency=0x4a3524936Db5C310d852266033589D3f6F30BA5d',
      },
      {
        label: 'Liquidity LP V1',
        href: 'https://exchange.goosedefi.com/#/add/0x4a3524936Db5C310d852266033589D3f6F30BA5d/BNB',
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Staking',
    icon: 'PoolIcon',
    href: '/staking',
  },
  {
    label: 'Forests',
    icon: 'ForestsIcon',
    href: '/pools',
  },
  {
    label: 'Lion-Jaguar LP',
    icon: 'LionIcon',
    href: 'https://lionswapdefi.com/farms',
  },
  {
    label: 'Mofi-Jaguar LP',
    icon: 'MoonIcon',
    href: 'https://mofi.finance/farms',
  },
  {
    label: 'Lime-Jaguar LP',
    icon: 'LimeIcon',
    href: 'https://limeswap.io/farms',
  },
  {
    label: 'Referrals',
    icon: 'GroupsIcon',
    href: '/referrals',
  },
  // {
  //   label: 'Referrals',
  //   icon: 'PoolIcon3',
  //   href: '/referrals',
  // },
  {
    label: 'Lottery',
    icon: 'TicketIcon',
    href: '/lottery',
  },
  // {
  //   label: 'NFT',
  //   icon: 'NftIcon',
  //   href: '/nft',
  // },
  {
    label: 'Price Charts',
    icon: 'InfoIcon',
    items: [
      {
        label: 'PooCoin',
        href: 'https://poocoin.app/tokens/0x4a3524936db5c310d852266033589d3f6f30ba5d',
      },
	  {
        label: 'DexGuru',
        href: 'https://dex.guru/token/0x4a3524936db5c310d852266033589d3f6f30ba5d-bsc',
      },
	  {
        label: 'BoggedFinance',
        href: 'https://charts.bogged.finance/?token=0x4a3524936Db5C310d852266033589D3f6F30BA5d',
      },
	  {
        label: 'DexTools',
        href: 'https://www.dextools.io/app/pancakeswap/pair-explorer/0xfd8f9c425863099df186ba9b7151d9456faf9222',
      },
    ],
  },
  {
    label: 'More',
    icon: 'MoreIcon',
    items: [
      {
        label: 'Github',
        href: 'https://github.com/JaguarSwap',
      },
      {
        label: 'Docs',
        href: 'https://jaguarswap.gitbook.io/jaguarswap/',
      },
    ],
  },
  {
    label: 'Listings',
    icon: 'ListingsIcon',
    items: [
      {
        label: 'CoinGecko',
        href: 'https://www.coingecko.com/en/coins/jaguarswap',
      },
	  {
        label: 'CoinMarketCap',
        href: 'https://coinmarketcap.com/currencies/jaguarswap/',
      },
	  {
        label: 'BscScan',
        href: 'https://bscscan.com/token/0x4a3524936Db5C310d852266033589D3f6F30BA5d',
      },
	  {
        label: 'Coinbase',
        href: 'https://www.coinbase.com/price/jaguarswap',
      },
	  {
        label: 'DappRadar',
        href: 'https://dappradar.com/binance-smart-chain/defi/jaguarswap',
      },
	  {
        label: 'Coinpaprika',
        href: 'https://coinpaprika.com/coin/jaguar-jaguarswap/',
      },
	  {
        label: 'LiveCoinWatch',
        href: 'https://www.livecoinwatch.com/price/JaguarSwap-JAGUAR',
      },
	  {
        label: 'BscProject',
        href: 'https://bscproject.org/#/project/768',
      },
	  {
        label: 'Vfat',
        href: 'https://vfat.tools/bsc/jaguar/',
      },
    ],
  },
  {
    label: 'Audits',
    icon: 'AuditIcon',
    href: 'https://jaguarswap.gitbook.io/jaguarswap/security/audit',
  },
  {
    label: 'Roadmap',
    icon: 'RoadmapIcon',
    href: 'https://jaguarswap.gitbook.io/jaguarswap/roadmap',
  },
  {
    label: 'Partnership',
    icon: 'HandshakeIcon',
    href: 'https://docs.google.com/forms/d/e/1FAIpQLSepN_bTQAOOflpoBPOa5aNV3SXuzr9Ome4E77XF7nXOtLgJYA/viewform?usp=sf_link',
  },
  {
    label: 'How to Start Farming',
    icon: 'SunIcon',
    href: 'https://jaguarswap.gitbook.io/jaguarswap/faqs/how-to-start-farming',
  },
]

export default config
