export const links = [
  {
    label: "Home",
    icon: "HomeIcon",
    href: "/",
  },
  {
    label: "Trade",
    icon: "TradeIcon",
    items: [
      {
        label: "Exchange",
        href: "https://exchange.pancakeswap.finance/#/swap?inputCurrency=ETH&outputCurrency=0x4a3524936Db5C310d852266033589D3f6F30BA5d",
      },
      {
        label: "Liquidity",
        href: "https://exchange.pancakeswap.finance/#/add/0x4a3524936Db5C310d852266033589D3f6F30BA5d/BNB",
      },
    ],
  },
  {
    label: "Farms",
    icon: "FarmIcon",
    href: "/farms",
  },
  {
    label: "Pools",
    icon: "PoolIcon",
    href: "/pools",
  },
];

export const socials = [
  {
    label: "Telegram",
    icon: "TelegramIcon",
    href: "https://t.me/jaguarswap_main",
  },
  {
    label: "Twitter",
    icon: "TwitterIcon",
    href: "https://twitter.com/JaguarSwap",
  },
  {
    label: "TelegramBot",
    icon: "RefIcon",
    href: "https://t.me/BinanceRocketJAGUAR",
  },
];


export const MENU_HEIGHT = 64;
export const MENU_ENTRY_HEIGHT = 48;
export const SIDEBAR_WIDTH_FULL = 240;
export const SIDEBAR_WIDTH_REDUCED = 49;
