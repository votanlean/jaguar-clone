import { Colors } from "./types";

export const baseColors = {
  failure: "#ED4B9E",
  primary: "#e7780c",
  primaryBright: "#fcbe9e",
  primaryDark: "#fcbe9e",
  secondary: "#fc8714",
  success: "#e1aff7",
  warning: "#FFB237",
};

export const brandColors = {
  binance: "#F0B90B",
};

export const lightColors: Colors = {
  ...baseColors,
  ...brandColors,
  background: "#282340",
  backgroundDisabled: "#ff8711",
  contrast: "#FFFFFF",
  invertedContrast: "#191326",
  input: "#483f5a",
  tertiary: "#353547",
  text: "#EAE2FC",
  textDisabled: "#666171",
  textSubtle: "#ff7e00",
  borderColor: "#524B63",
  card: "#3d334b",
  gradients: {
    bubblegum: "linear-gradient(139.73deg, #313D5C 0%, #3D2A54 100%)",
  },
};

export const darkColors: Colors = {
  ...baseColors,
  ...brandColors,
  secondary: "#fc8714",
  background: "#282340",
  backgroundDisabled: "#ff8711",
  contrast: "#FFFFFF",
  invertedContrast: "#191326",
  input: "#483f5a",
  primaryDark: "#0098A1",
  tertiary: "#353547",
  text: "#EAE2FC",
  textDisabled: "#666171",
  textSubtle: "#ff7e00",
  borderColor: "#524B63",
  card: "#3d334b",
  gradients: {
    bubblegum: "linear-gradient(139.73deg, #313D5C 0%, #3D2A54 100%)",
  },
};
